package com.ruddi.noise;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class Mathematics {
    public static int generate(int sigma) {
        double x = 0.0, y = 0.0, s = 0.0;
        while (s == 0.0 || s > 1.0) {
            x = 2.0 * Math.random() - 1;
            y = 2.0 * Math.random() - 1;
            s = x * x + y * y;
        }

        double res = x * Math.sqrt(-2.0 * Math.log(s) / s);
        return (int) (res * sigma);
    }

    public static int clip(int I, int N) {
        if (I + N < 0)
            return 0;

        else if (I + N >= 256)
            return 255;

        return I + N;
    }

    public static int randComponent(int I, double pa, double pb) {
        int newI;
        double gen = ThreadLocalRandom.current().nextDouble(0, 1);

        if (gen >= 0.00 && gen <= pa)
            return 0;

        else if (gen >= pa && gen <= pa + pb)
            return 255;

        return I;
    }

    public static int clipping(int value, int leftBorder, int rightBorder) {
        if(value > rightBorder)
            return rightBorder;
        else if(value < leftBorder)
            return leftBorder;

        return value;
    }
}
