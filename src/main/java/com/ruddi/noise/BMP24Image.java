package com.ruddi.noise;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.*;
import java.util.ArrayList;

public class BMP24Image {
    private Pixel[][] pixels;
    private final int BFLEN = 14;
    private final int BILEN = 40;
    private byte[] BITMAPINFOHEADER = new byte[BILEN];
    private byte[] BITMAPFILEHEADER = new byte[BFLEN];
    private final int HEIGHT;
    private final int WIDTH;
    private int FILESIZE;

    public BMP24Image(int HEIGHT, int WIDTH) {
        this.HEIGHT = HEIGHT;
        this.WIDTH = WIDTH;

        pixels = new Pixel[HEIGHT][WIDTH];
    }

    public BMP24Image(BMP24Image copy) {
        this.BITMAPINFOHEADER = copy.BITMAPINFOHEADER.clone();
        this.BITMAPFILEHEADER = copy.BITMAPFILEHEADER.clone();
        this.HEIGHT = copy.HEIGHT;
        this.WIDTH = copy.WIDTH;
        this.FILESIZE = copy.FILESIZE;

        this.pixels = new Pixel[copy.getHeight()][copy.getWidth()];
        for (int i = 0; i < getHeight(); ++i) {
            for (int j = 0; j < getWidth(); ++j) {
                this.pixels[i][j] = copy.pixels[i][j];
            }
        }
    }

    public BMP24Image(String filename) throws IOException {
        FileInputStream fs = new FileInputStream(filename);

        fs.read(BITMAPFILEHEADER, 0, BFLEN);
        fs.read(BITMAPINFOHEADER, 0, BILEN);

        FILESIZE = ((int) BITMAPFILEHEADER[5] << 24)
                | ((int) BITMAPFILEHEADER[4] << 16)
                | ((int) BITMAPFILEHEADER[3] << 8)
                | ((int) BITMAPFILEHEADER[2]);

        WIDTH = ((int) BITMAPINFOHEADER[7] << 24)
                | ((int) BITMAPINFOHEADER[6] << 16)
                | ((int) BITMAPINFOHEADER[5] << 8)
                | ((int) BITMAPINFOHEADER[4]);

        HEIGHT = ((int) BITMAPINFOHEADER[11] << 24)
                | ((int) BITMAPINFOHEADER[10] << 16)
                | ((int) BITMAPINFOHEADER[9] << 8)
                | ((int) BITMAPINFOHEADER[8]);

        pixels = new Pixel[HEIGHT][WIDTH];

        int IMAGESIZE = ((int) BITMAPINFOHEADER[23] << 24)
                | ((int) BITMAPINFOHEADER[22] << 16)
                | ((int) BITMAPINFOHEADER[21] << 8)
                | ((int) BITMAPINFOHEADER[20]);

        byte[] RGBDATA = new byte[WIDTH * 3 * HEIGHT];
        fs.read(RGBDATA, 0, WIDTH * 3 * HEIGHT);
        int currentPixel = 0;
        for (int i = 0; i < HEIGHT; ++i) {
            for (int j = 0; j < WIDTH; ++j) {
                int RED = (int) RGBDATA[currentPixel + 2] & 0xFF;
                int GREEN = (int) RGBDATA[currentPixel + 1] & 0xFF;
                int BLUE = (int) RGBDATA[currentPixel] & 0xFF;
                pixels[i][j] = new Pixel(RED, GREEN, BLUE);
                currentPixel += 3;
            }
        }
    }

    public int getHeight() {
        return HEIGHT;
    }

    public int getWidth() {
        return WIDTH;
    }

    public Pixel getPixel(int i, int j) {
        return pixels[i][j];
    }

    public void setPixel(int i, int j, Pixel p) {
        pixels[i][j] = p;
    }

    public ArrayList<Integer> convertComponentToArray() {
        ArrayList<Integer> res = new ArrayList<>();
        for (int i = 0; i < HEIGHT; ++i) {
            for (int j = 0; j < WIDTH; ++j) {
                res.add(getPixel(i, j).getRed());
            }
        }
        return res;
    }

    public void writeImage(String filename) {
        final BufferedImage res = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
        for (int i = 0; i < HEIGHT; ++i) {
            for (int j = 0; j < WIDTH; ++j) {
                res.setRGB(j, i, getPixel(HEIGHT - 1 - i, j).getColor().getRGB());
            }
        }

        try {
            ImageIO.write(res, "bmp", new File(filename));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeImage2(String filename) {
        byte[] res = new byte[FILESIZE];
        for (int i = 0; i < 14; ++i) {
            res[i] = BITMAPFILEHEADER[i];
        }

        for (int i = 14; i < 54; ++i) {
            res[i] = BITMAPINFOHEADER[i - BFLEN];
        }

        int j = 0, k = 0;
        for (int i = 54; i < FILESIZE; i += 3) {
            res[i] = (byte) getPixel(j, k).getBlue();
            res[i + 1] = (byte) getPixel(j, k).getGreen();
            res[i + 2] = (byte) getPixel(j, k).getRed();
            k++;
            if (k >= WIDTH) {
                k = 0;
                j++;
            }

            if (j >= HEIGHT) {
                break;
            }
        }

        try (FileOutputStream fos = new FileOutputStream(filename)) {
            fos.write(res);
            //fos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static BMP24Image sum(BMP24Image i1, BMP24Image i2) {
        BMP24Image res = new BMP24Image(i1);

        for (int i = 0; i < i1.getHeight(); ++i) {
            for (int j = 0; j < i1.getWidth(); ++j) {
                int newY = i1.getPixel(i, j).getRed() +
                        i2.getPixel(i, j).getRed();
                newY = Mathematics.clipping(newY, 0, 255);
                Pixel pixel = new Pixel(newY, newY, newY);
                res.setPixel(i, j, pixel);
            }
        }

        return res;
    }

    public void clipping(int shift) {
        for (int i = 0; i < getHeight(); ++i) {
            for (int j = 0; j < getWidth(); ++j) {
                int newI = Mathematics.clipping(
                        getPixel(i, j).getRed() + shift, 0, 255);
                setPixel(i, j, new Pixel(newI, newI, newI));
            }
        }
    }

    public static double average(BMP24Image img) {
        int sum = 0;
        int count = img.getWidth() * img.getHeight();
        for (int i = 0; i < img.getHeight(); ++i) {
            for (int j = 0; j < img.getWidth(); ++j) {
                sum += img.getPixel(i, j).getRed();
            }
        }
        return (double)(sum / count);
    }

    public void writeI(String filename) throws Exception {
        BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
        for (int i = 0; i < getHeight(); ++i) {
            for (int j = 0; j < getWidth(); ++j) {
                bw.write(getPixel(i, j).getRed() + "\n");
            }
        }
    }

    public void changeY(int shift) {
        for (int i = 0; i < getHeight(); ++i) {
            for (int j = 0; j < getWidth(); ++j) {
                int newI = getPixel(i, j).getRed();
                newI += shift;
                newI = Mathematics.clipping(newI, 0, 255);
                setPixel(i, j, new Pixel(newI, newI, newI));
            }
        }
    }
}
