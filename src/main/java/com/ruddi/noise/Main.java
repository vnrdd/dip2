package com.ruddi.noise;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Main {
    public static void main(String[] args) throws Exception {
        BMP24Image source = new BMP24Image("res/airplane.bmp");

        // RGB to III
        BMP24Image imgI = Analysis.convertToI(source);
        imgI.writeImage2("res/I.bmp");


//        Analysis.movingAvg(Analysis.additionNoise(imgI, 30), 1)
//                .writeImage2("res/for_report/MA30_1.bmp");
//        Analysis.movingAvg(Analysis.additionNoise(imgI, 30), 3)
//                .writeImage2("res/for_report/MA30_3.bmp");;
//        Analysis.movingAvg(Analysis.additionNoise(imgI, 30), 5)
//                .writeImage2("res/for_report/MA30_5.bmp");;


        // ADDITION NOISE
//        Analysis.ANPSNRanalysis(imgI);
//
//        // IMPULSE NOISE
//        Analysis.INPSNRanalysis(imgI);
//
//        // MOVING AVERAGE
//        System.out.print("~ ~ ~ MOVING AVERAGE ~ ~ ~\nSIGMA = 1 // ");
//        Analysis.maPSNRMax(imgI, 1);
//
//        System.out.print("SIGMA = 10 // ");
//        Analysis.maPSNRMax(imgI, 10);
//
//        System.out.print("SIGMA = 30 // ");
//        Analysis.maPSNRMax(imgI, 30);
//
//        System.out.print("SIGMA = 50 // ");
//        Analysis.maPSNRMax(imgI, 50);
//
//        System.out.print("SIGMA = 80 // ");
//        Analysis.maPSNRMax(imgI, 80);
//
//        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ `");
//
        // GAUSSIAN BLUR
        System.out.print("\n~ ~ ~ GAUSSIAN BLUR ~ ~ ~\nSIGMA = 1 // ");
//        Analysis.gbPSNRMax(imgI, 1, 1);
//        Analysis.gbPSNRMax(imgI, 3, 1);
//        Analysis.gbPSNRMax(imgI, 5, 1);
//
//        System.out.print("SIGMA = 10 // ");
//        Analysis.gbPSNRMax(imgI, 1, 10);
//        Analysis.gbPSNRMax(imgI, 3, 10);
//        Analysis.gbPSNRMax(imgI, 5, 10);
//
//        System.out.print("SIGMA = 30 // ");
//        Analysis.gbPSNRMax(imgI, 1, 30);
//        Analysis.gbPSNRMax(imgI, 3, 30);
//        Analysis.gbPSNRMax(imgI, 5, 30);
//
//        System.out.print("SIGMA = 50 // ");
//        Analysis.gbPSNRMax(imgI, 1, 50);
//        Analysis.gbPSNRMax(imgI, 3, 50);
//        Analysis.gbPSNRMax(imgI, 5, 50);
//
//        System.out.print("SIGMA = 80 // ");
//        Analysis.gbPSNRMax(imgI, 1, 80);
//        Analysis.gbPSNRMax(imgI, 3, 80);
//        Analysis.gbPSNRMax(imgI, 5, 80);

        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");
//
//        // MEDIAN FILTER
//        System.out.print("\n~ ~ ~ MEDIAN FILTER ~ ~ ~\nSIGMA = 1 // ");
//        Analysis.mfPSNRMax(imgI, 1);
//
//        System.out.print("SIGMA = 10 // ");
//        Analysis.mfPSNRMax(imgI, 10);
//
//        System.out.print("SIGMA = 30 // ");
//        Analysis.mfPSNRMax(imgI, 30);
//
//        System.out.print("SIGMA = 50 // ");
//        Analysis.mfPSNRMax(imgI, 50);
//
//        System.out.print("SIGMA = 80 // ");
//        Analysis.mfPSNRMax(imgI, 80);
//
//        System.out.println("~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~ ~");

        // 5.1
//        BMP24Image pc5 = Analysis.impulseNoise(imgI, 0.025, 0.025);
//        BMP24Image pc10 = Analysis.impulseNoise(imgI, 0.05, 0.05);
//        BMP24Image pc25 = Analysis.impulseNoise(imgI, 0.125, 0.125);
//        BMP24Image pc50 = Analysis.impulseNoise(imgI, 0.25, 0.25);
//
//        pc5.writeImage2("res/impulse_noise/05.bmp");
//        pc10.writeImage2("res/impulse_noise/10.bmp");
//        pc25.writeImage2("res/impulse_noise/25.bmp");
//        pc50.writeImage2("res/impulse_noise/50.bmp");
//
//        // 5.2
//        System.out.println("5% PSNR: " + Analysis.generalPSNR(imgI, pc5));
//        System.out.println("10% PSNR: " + Analysis.generalPSNR(imgI, pc10));
//        System.out.println("25% PSNR: " + Analysis.generalPSNR(imgI, pc25));
//        System.out.println("50% PSNR: " + Analysis.generalPSNR(imgI, pc50));
//
//        // 5.4
//        Analysis.impAnalysis(imgI, pc5, "pc5.txt");
//        Analysis.impAnalysis(imgI, pc10, "pc10.txt");
//        Analysis.impAnalysis(imgI, pc25, "pc25.txt");
//        Analysis.impAnalysis(imgI, pc50, "pc50.txt");
//
//
//        // 2 PART
//        double[][] w = {
//                {0, -1, 0},
//                {-1, 4, -1},
//                {0, -1, 0}
//        };
//
//        imgI.writeI("res/laplass/histoOrig.txt");
//
//        BMP24Image high = Analysis.laplassOperator(imgI, w, 1.0);
//        high.clipping(0);
//        //high.writeImage2("res/laplass/defLaplass.bmp");
//
//        (BMP24Image.sum(imgI, high)).writeImage2("res/laplass/highFreqDef.bmp");
//
//        double original = w[1][1];
//        double[] values = {1.0, 1.1, 1.2, 1.3, 1.4, 1.5};
//        for (double alpha : values) {
//            w[1][1] += alpha;
//            high = Analysis.laplassOperator(imgI, w, alpha);
//            high.clipping(0);
//            high.writeImage2("res/laplass/highFreq" + alpha + ".bmp");
//            w[1][1] = original;
//            high.writeI("res/laplass/histo" + alpha + ".txt");
//            System.out.println(alpha + " average: " + BMP24Image.average(high));
//        }
//
//        // SOBEL
//
//        double[][] h = {
//                {-1, 0, 1},
//                {-2, 0, 2},
//                {-1, 0, 1}
//        };
//        double[][] v = {
//                {1, 2, 1},
//                {0, 0, 0},
//                {-1, -2, -1}
//        };
////
//        BMP24Image Gh = Analysis.laplassOperator(imgI, h, 1.0);
//        Gh.clipping(0);
//        Gh.writeImage2("res/sobel/sobel1.bmp");
//        BMP24Image Gv = Analysis.laplassOperator(imgI, v, 1.0);
//        Gv.clipping(0);
//        Gv.writeImage2("res/sobel/sobel2.bmp");
//
//        double[][] dI = new double[Gh.getHeight()][Gh.getWidth()];
////
//        BMP24Image sobelRes = new BMP24Image(imgI);
//        BMP24Image map = new BMP24Image(imgI);
//        for(int i = 0; i < sobelRes.getHeight(); ++i) {
//            for(int j = 0; j < sobelRes.getWidth(); ++j) {
//                int GhP = Gh.getPixel(i, j).getRed();
//                int GvP = Gv.getPixel(i, j).getRed();
//
//                double newI = Math.sqrt(Math.pow(GhP, 2) + Math.pow(GvP, 2));
//                newI = Mathematics.clipping((int)newI, 0, 255);
//
//                dI[i][j] = newI;
//                sobelRes.setPixel(i, j, new Pixel((int)newI, (int)newI, (int)newI));
//
//                Pixel p = new Pixel(0, 0, 0);
//                if(GhP > 0) {
//                    if(GvP > 0)
//                        p.setColor(255, 0, 0);
//                    else
//                        p.setColor(255, 255, 255);
//                }
//                else {
//                    if(GvP > 0)
//                        p.setColor(0, 0, 255);
//                    else
//                        p.setColor(0, 255, 0);
//                }
//                map.setPixel(i, j, p);
//            }
//        }
//
       // sobelRes.writeImage2("res/sobel/sobelLI.bmp");
//        map.writeImage2("res/sobel/map.bmp");
//
//        BMP24Image thr = new BMP24Image(imgI);
//        for(int i = 0; i < dI.length; ++i) {
//            for(int j = 0; j < dI[i].length; ++j) {
//                double value = dI[i][j];
//                int newI = 0;
//                if(value <= 50)
//                    newI = 255;
//                thr.setPixel(i, j, new Pixel(newI, newI, newI));
//            }
//        }
//        thr.writeImage2("res/sobel/thr50.bmp");
//
//        for(int i = 0; i < dI.length; ++i) {
//            for(int j = 0; j < dI[i].length; ++j) {
//                double value = dI[i][j];
//                int newI = 0;
//                if(value <= 100)
//                    newI = 255;
//                thr.setPixel(i, j, new Pixel(newI, newI, newI));
//            }
//        }
//        thr.writeImage2("res/sobel/thr100.bmp");
//
//        for(int i = 0; i < dI.length; ++i) {
//            for(int j = 0; j < dI[i].length; ++j) {
//                double value = dI[i][j];
//                int newI = 0;
//                if(value <= 200)
//                    newI = 255;
//                thr.setPixel(i, j, new Pixel(newI, newI, newI));
//            }
//        }
//        thr.writeImage2("res/sobel/thr200.bmp");
//
//        // 2.3.1
//
        BMP24Image light = new BMP24Image(imgI);
        light.changeY(40);
        light.writeImage2("res/gammas/light.bmp");
        light.writeI("res/align/origLight.txt");
        BMP24Image dark = new BMP24Image(imgI);
        dark.changeY(-40);
        dark.writeImage2("res/gammas/dark.bmp");
        dark.writeI("res/align/origDark.txt");

        imgI.writeI("res/align/origNormal.txt");
//
//        // 2.3.2
//
        Analysis.gammaTransition(imgI, "normal");
        Analysis.gammaTransition(light, "light");
        Analysis.gammaTransition(dark, "dark");

        Analysis.aligningHistogram(dark, "dark");
        Analysis.aligningHistogram(light, "light");
        Analysis.aligningHistogram(imgI, "normal");

        BMP24Image bordered = new BMP24Image(imgI);
        for(int T = 16; T <= 240; T += 28) {
            int newY = 0;
            for(int i = 0; i < imgI.getHeight(); ++i){
                for(int j = 0; j < imgI.getWidth(); ++j) {
                    if(imgI.getPixel(i, j).getRed() > T)
                        newY = 0;
                    else
                        newY = 255;
                    bordered.setPixel(i, j, new Pixel(newY, newY, newY));
                }
            }
            bordered.writeImage2("res/border/" + T + ".bmp");
        }
    }
}
