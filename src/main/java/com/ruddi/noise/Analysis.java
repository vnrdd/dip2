package com.ruddi.noise;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.*;

public class Analysis {
    public static BMP24Image convertToI(BMP24Image img) {
        BMP24Image converted = new BMP24Image(img);

        for (int i = 0; i < img.getHeight(); ++i) {
            for (int j = 0; j < img.getWidth(); ++j) {
                Pixel buf = img.getPixel(i, j);
                int I = (int) (0.299 * (double) buf.getRed()
                        + 0.587 * (double) buf.getGreen()
                        + 0.114 * (double) buf.getBlue());
                converted.setPixel(i, j, new Pixel(I, I, I));
            }
        }

        return converted;
    }

    public static BMP24Image additionNoise(BMP24Image src, int sigma) {
        BMP24Image noised = new BMP24Image(src);


        for (int i = 0; i < src.getHeight(); ++i) {
            for (int j = 0; j < src.getWidth(); ++j) {
                Pixel buf = src.getPixel(i, j);
                int oldI = buf.getRed();
                int noise = Mathematics.generate(sigma);
                int newI = Mathematics.clip(oldI, noise);

                noised.setPixel(i, j, new Pixel(newI, newI, newI));
            }
        }

        return noised;
    }

    public static BMP24Image impulseNoise(BMP24Image src, double pa, double pb) {
        BMP24Image noised = new BMP24Image(src);

        for (int i = 0; i < src.getHeight(); ++i) {
            for (int j = 0; j < src.getWidth(); ++j) {
                Pixel buf = src.getPixel(i, j);
                int oldI = buf.getRed();
                int newI = Mathematics.randComponent(oldI, pa, pb);

                noised.setPixel(i, j, new Pixel(newI, newI, newI));
            }
        }

        return noised;
    }

    public static void ANPSNRanalysis(BMP24Image src) throws Exception {
        BufferedWriter bw = new BufferedWriter(
                new FileWriter("res/addition_noise/PSNR.txt"));

        int[] sigmas = {1, 10, 30, 50, 80};

        for (int sigma : sigmas) {
            bw.write(sigma + " " + PSNR(src, sigma) + "\n");
        }

        bw.close();
    }

    public static void INPSNRanalysis(BMP24Image src) throws Exception {
        BufferedWriter bw = new BufferedWriter(
                new FileWriter("res/impulse_noise/PSNR.txt"));

        double[] ps = {0.025, 0.05, 0.125, 0.25};

        for (double p : ps) {
            String buf = "" + PSNR(src, p, p);
            bw.write(p + " " + buf + "\n");
        }

        bw.close();
    }

    public static double PSNR(BMP24Image src, int sigma) {
        BMP24Image addNoised = Analysis.additionNoise(src, sigma);
        addNoised.writeImage2("res/addition_noise/" + sigma + ".bmp");

        return generalPSNR(src, addNoised);
    }

    public static double PSNR(BMP24Image src, double pa, double pb) {
        BMP24Image impNoised = Analysis.impulseNoise(src, pa, pb);
        String buf = pa + "_" + pb;
        buf = buf.replaceAll("\\.", "");
        impNoised.writeImage2("res/impulse_noise/" + buf + ".bmp");

        return generalPSNR(src, impNoised);
    }

    public static double generalPSNR(BMP24Image original, BMP24Image noised) {
        long top = (long) (original.getHeight() * original.getWidth()
                * Math.pow(Math.pow(2, 8) - 1, 2));
        double down = 0.0;

        for (int i = 0; i < original.getHeight(); ++i) {
            for (int j = 0; j < original.getWidth(); ++j) {
                Pixel srcP = original.getPixel(i, j);
                Pixel noisedP = noised.getPixel(i, j);

                down += Math.pow(srcP.getRed() - noisedP.getRed(), 2);
            }
        }

        return 10 * Math.log10(top / down);
    }

    public static BMP24Image movingAvg(BMP24Image src, int R) {
        BMP24Image res = new BMP24Image(src);
        int down = (int) Math.pow(2 * R + 1, 2);

        for (int i = 0; i < src.getHeight(); ++i) {
            for (int j = 0; j < src.getWidth(); ++j) {
                int sum = 0;
                for (int k = -R; k <= R; ++k) {
                    int y = i + k;
                    for (int m = -R; m <= R; ++m) {
                        int x = j + m;
                        if (x >= 0 && x < src.getWidth() && y >= 0 && y < src.getHeight())
                            sum += src.getPixel(y, x).getRed();
                    }
                }
                res.setPixel(i, j, new Pixel(sum / down, sum / down, sum / down));
            }
        }
        return res;
    }

    public static BMP24Image gaussianBlur(BMP24Image src, int R, double sigma) {
        BMP24Image res = new BMP24Image(src);
        double down = 0.0;

        for (int i = 0; i < src.getHeight(); ++i) {
            for (int j = 0; j < src.getWidth(); ++j) {
                int sum = 0;
                for (int k = -R; k <= R; ++k) {
                    int y = i + k;
                    for (int m = -R; m <= R; ++m) {
                        int x = j + m;
                        double up = -(k * k + m * m);
                        double bot = 2.0 * sigma * sigma;
                        double w = Math.exp(up / bot);
                        if (x >= 0 && x < src.getWidth() && y >= 0 && y < src.getHeight()) {
                            sum += w * src.getPixel(y, x).getRed();
                            down += w;
                        }
                    }
                }
                res.setPixel(i, j, new Pixel((int) (sum / down),
                        (int) (sum / down), (int) (sum / down)));
                down = 0;
            }
        }
        return res;
    }

    public static BMP24Image medianFilter(BMP24Image src, int R) {
        BMP24Image res = new BMP24Image(src);

        for (int i = 0; i < src.getHeight(); ++i) {
            for (int j = 0; j < src.getWidth(); ++j) {
                List<Integer> window = new ArrayList<>();
                for (int k = -R; k <= R; ++k) {
                    int y = i + k;
                    for (int m = -R; m <= R; ++m) {
                        int x = j + m;
                        if (x >= 0 && x < src.getWidth() && y >= 0 && y < src.getHeight())
                            window.add(src.getPixel(y, x).getRed());
                    }
                }
                Collections.sort(window);
                int newI = window.get(window.size() / 2);
                res.setPixel(i, j, new Pixel(newI, newI, newI));
            }
        }
        return res;
    }

    public static void maPSNRMax(BMP24Image src, int sigma) throws Exception {
        BMP24Image noised = Analysis.additionNoise(src, sigma);
        double maxPSNR = 0;
        int optR = 0;

        BufferedWriter bw = new BufferedWriter(
                new FileWriter("res/psnr_analysis/42PSNR_" + sigma + ".txt"));

        for (int R = 1; R <= 10; ++R) {
            double PSNR = Analysis.generalPSNR(src, Analysis.movingAvg(noised, R));
            bw.write(R + " " + PSNR + "\n");
            if (PSNR >= maxPSNR) {
                maxPSNR = PSNR;
                optR = R;
            }
        }

        BMP24Image toWrite = Analysis.movingAvg(noised, optR);
        toWrite.writeImage2("res/MA_" + sigma + "_" + optR + ".bmp");
        bw.close();
        System.out.println("R = " + optR + ", PSNR = " + maxPSNR);
    }

    public static void gbPSNRMax(BMP24Image src, int R, int sigma) throws Exception {
        BMP24Image noised = Analysis.additionNoise(src, sigma);
        double maxPSNR = 0;
        double optS = 0;

        BufferedWriter bw = new BufferedWriter(
                new FileWriter("res/psnr_analysis/44PSNR_" + sigma + "_" + R + ".txt"));

        double[] s = {0.1, 0.25, 0.5, 0.75, 1.0, 1.25, 1.5, 2.0};
        for (double ss : s) {
            double PSNR = Analysis.generalPSNR(src, Analysis.gaussianBlur(noised, R, ss));
            bw.write(ss + " " + PSNR + "\n");
            if (PSNR >= maxPSNR) {
                maxPSNR = PSNR;
                optS = ss;
            }
        }

        BMP24Image toWrite = Analysis.gaussianBlur(noised, R, optS);
        toWrite.writeImage2("res/GB_" + sigma + "_" + R + "_" + optS + ".bmp");
        bw.close();
        System.out.println("sigma = " + optS + ", PSNR = " + maxPSNR);
    }

    public static void mfPSNRMax(BMP24Image src, int sigma) throws Exception {
        BMP24Image noised = Analysis.additionNoise(src, sigma);
        double maxPSNR = 0;
        int optR = 0;

        BufferedWriter bw = new BufferedWriter(
                new FileWriter("res/psnr_analysis/48PSNR_" + sigma + ".txt"));

        for (int R = 1; R <= 5; ++R) {
            double PSNR = Analysis.generalPSNR(src, Analysis.medianFilter(noised, R));
            bw.write(R + " " + PSNR + "\n");
            if (PSNR >= maxPSNR) {
                maxPSNR = PSNR;
                optR = R;
            }
        }

        BMP24Image toWrite = Analysis.medianFilter(noised, optR);
        toWrite.writeImage2("res/MF_" + sigma + "_" + optR + ".bmp");
        bw.close();
        System.out.println("R = " + optR + ", PSNR = " + maxPSNR);
    }

    public static void impAnalysis(BMP24Image src, BMP24Image noised, String filename)
            throws Exception {
        BufferedWriter bw = new BufferedWriter(
                new FileWriter("res/impulse_noise/" + filename));

        String finalFilename = filename.substring(0, filename.length() - 4);
        BMP24Image filtered = null;
        int optR = 0;
        double maxPSNR = 0.0;

        for (int R = 1; R <= 5; ++R) {
            BMP24Image buf = Analysis.medianFilter(noised, R);
            double PSNR = Analysis.generalPSNR(src, buf);
            bw.write(R + " " + PSNR + "\n");

            if (PSNR > maxPSNR) {
                maxPSNR = PSNR;
                optR = R;
                filtered = new BMP24Image(buf);
            }
        }

        assert filtered != null;
        filtered.writeImage2("res/impulse_noise/filtered_" + finalFilename
                + "_" + optR + ".bmp");
        bw.close();
    }

    public static BMP24Image laplassOperator(BMP24Image src, double[][] w, double alpha) {
        BMP24Image res = new BMP24Image(src);

        for (int i = 0; i < src.getHeight(); ++i) {
            for (int j = 0; j < src.getWidth(); ++j) {
                double sum = 0;
                double Z = 0;
                for (int k = 0; k < w.length; ++k) {
                    int y = i + k;
                    if (y < 0) y = 0;
                    if (y > src.getHeight() - 1) y = src.getHeight() - 1;
                    for (int m = 0; m < w[0].length; ++m) {
                        int x = j + m;
                        if (x < 0) x = 0;
                        if (x > src.getWidth() - 1) x = src.getWidth() - 1;

                        Z += w[k][m];
                        sum += src.getPixel(y, x).getRed() * w[k][m];
                    }
                }
                int newI;
                if (w[1][1] > 0)
                    newI = (int) ((alpha - 1) * src.getPixel(i, j).getRed() + sum);
                else
                    newI = (int) ((alpha - 1) * src.getPixel(i, j).getRed() - sum);

                if (Z != 0)
                    newI /= Z;

                res.setPixel(i, j, new Pixel(newI,
                        newI, newI));
                Z = 0;
            }
        }
        return res;
    }

    public static void gammaTransition(BMP24Image src, String filename) throws Exception {
        double c = 1.0;
        double[] y = {0.1, 0.6, 1.5, 6.0};
        for (double gamma : y) {
            BufferedWriter bw = new BufferedWriter(
                    new FileWriter("res/gammas/" + filename + gamma + ".txt"));
            Map<Integer, Integer> map = new HashMap<>();
            for (int i = 0; i < src.getHeight(); ++i) {
                for (int j = 0; j < src.getWidth(); ++j) {
                    double value = src.getPixel(i, j).getRed();
                    value /= 255;
                    value = Math.pow(value, gamma);
                    value *= c;
                    value *= 255;
                    //value = Mathematics.clipping((int) value, 0, 255);
                    map.put(src.getPixel(i, j).getRed(), (int) value);
                }
            }
            for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
                bw.write(entry.getKey() + " " + entry.getValue() + "\n");
            }

            BMP24Image result = new BMP24Image(src);
            BufferedWriter bw2 = new BufferedWriter(
                    new FileWriter("res/gammas/gist_" + filename + gamma + ".txt"));

            for (int i = 0; i < result.getHeight(); ++i) {
                for (int j = 0; j < result.getWidth(); ++j) {
                    int key = src.getPixel(i, j).getRed();
                    int newI = map.get(key);
                    result.setPixel(i, j, new Pixel(newI, newI, newI));
                    bw2.write(newI + "\n");
                }
            }

            bw.close();
            bw2.close();
        }
    }

    public static void aligningHistogram(BMP24Image image, String filename)
            throws Exception {
        int[] frequences = new int[256];

        BufferedWriter bw = new BufferedWriter(new FileWriter("res/align/"
                + filename + ".txt"));

        for (int i = 0; i < image.getHeight(); ++i) {
            for (int j = 0; j < image.getWidth(); ++j) {
                int Y = image.getPixel(i, j).getRed();
                ++frequences[Y];
            }
        }

        BMP24Image res = new BMP24Image(image);

        for (int i = 0; i < image.getHeight(); ++i) {
            for (int j = 0; j < image.getWidth(); ++j) {
                int sum = 0;
                for (int k = 0; k < image.getPixel(i, j).getRed(); ++k)
                    sum += frequences[k];
                int newY = (int) (255 * sum / (image.getWidth() * image.getHeight()));
                res.setPixel(i, j, new Pixel(newY, newY, newY));
                bw.write(newY + "\n");
            }
        }
        res.writeImage2("res/align/"
                + filename + ".bmp");
        bw.close();
    }
}
