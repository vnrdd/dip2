package com.ruddi.noise;

import java.awt.*;

public class Pixel {
    private int RED;
    private int GREEN;
    private int BLUE;

    public Pixel(int RED, int GREEN, int BLUE){
        this.RED = RED;
        this.GREEN = GREEN;
        this.BLUE = BLUE;
    }

    public Pixel(Pixel copy) {
        this.RED = copy.RED;
        this.GREEN = copy.GREEN;
        this.BLUE = copy.BLUE;
    }

    public int getRed(){
        return RED;
    }

    public int getGreen(){
        return GREEN;
    }

    public int getBlue(){
        return BLUE;
    }

    public void setColor(int RED, int GREEN, int BLUE){
        this.RED = RED;
        this.GREEN = GREEN;
        this.BLUE = BLUE;
    }

    public Color getColor(){
        return (new Color(RED, GREEN, BLUE));
    }

    @Override
    public String toString(){
        StringBuilder sb = new StringBuilder();
        sb.append(Integer.toString(RED));
        sb.append(" ");
        sb.append(Integer.toString(GREEN));
        sb.append(" ");
        sb.append(Integer.toString(BLUE));

        return sb.toString();
    }
}
