clear;
close all;


data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/addition_noise/PSNR.txt");
sigma = data(:, 1);
psnr = data(:, 2);


figure(1);
subplot(2,1,1);
plot(sigma, psnr, '-bo');
xlabel("sigma");
ylabel("psnr");
title('Addition Noise PSNR');
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/impulse_noise/PSNR.txt");
p = data(:, 1);
psnr = data(:, 2);

subplot(2,1,2);
plot(p, psnr, '-ro');
xlabel("p");
ylabel("psnr");
title('Impulse Noise PSNR');
hold on;
grid on;

%% OPTIMAL CHARACTERISTICS FOR PSNR
figure(2);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/42PSNR_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/42PSNR_10.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/42PSNR_30.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/42PSNR_50.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/42PSNR_80.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

xlabel('R');
ylabel('PSNR');
legend('SIGMA = 1', 'SIGMA = 10', 'SIGMA = 30', 'SIGMA = 50', 'SIGMA = 80');
title('PSNR DEPENDENCE OF R (4.2)');



figure(3);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_1_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_10_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_30_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_50_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_80_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

xlabel('sigma');
ylabel('PSNR');
legend('SIGMA = 1', 'SIGMA = 10', 'SIGMA = 30', 'SIGMA = 50', 'SIGMA = 80');
title('PSNR DEPENDENCE OF sigma (4.4) R = 1');

%%

figure(4);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_1_3.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_10_3.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_30_3.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_50_3.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_80_3.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

xlabel('sigma');
ylabel('PSNR');
legend('SIGMA = 1', 'SIGMA = 10', 'SIGMA = 30', 'SIGMA = 50', 'SIGMA = 80');
title('PSNR DEPENDENCE OF sigma (4.4) R = 3');

%%

figure(5);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_1_5.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_10_5.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_30_5.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_50_5.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/44PSNR_80_5.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

xlabel('sigma');
ylabel('PSNR');
legend('SIGMA = 1', 'SIGMA = 10', 'SIGMA = 30', 'SIGMA = 50', 'SIGMA = 80');
title('PSNR DEPENDENCE OF sigma (4.4) R = 5');

%%

figure(6);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/48PSNR_1.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/48PSNR_10.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/48PSNR_30.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/48PSNR_50.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/48PSNR_80.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

xlabel('R');
ylabel('PSNR');
legend('SIGMA = 1', 'SIGMA = 10', 'SIGMA = 30', 'SIGMA = 50', 'SIGMA = 80');
title('PSNR DEPENDENCE OF R (4.8)');

%% 5.4


figure(7);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/impulse_noise/pc5.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/impulse_noise/pc10.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/impulse_noise/pc25.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/impulse_noise/pc50.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

xlabel('R');
ylabel('');
title("MEDIAN FILTER FOR IMPULSE NOISED IMAGES");
legend('5%', '10%', '25%', '50%');

%% 4.10

figure(8);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/addition_noise/PSNR.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/bestMA.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/bestGF.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/psnr_analysis/bestMF.txt");
R = data(:, 1);
psnr = data(:, 2);

plot(R, psnr);
hold on;
grid on;



xlabel('sigma');
ylabel('PSNR');
title("MEDIAN FILTER FOR IMPULSE NOISED IMAGES");
legend('Noise', 'Moving Average', 'Gauss Filter', 'Median Filter');

%% LAPLASS

figure(9);

subplot(4, 2, 1);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histoOrig.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;
title('Original');

subplot(4, 2, 2);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histo1.0.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;
title('alpha = 1.0');

subplot(4, 2, 3);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histo1.1.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;
title('alpha = 1.1');

subplot(4, 2, 4);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histo1.2.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;

title('alpha = 1.2');

subplot(4, 2, 5);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histo1.3.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;

title('alpha = 1.3');

subplot(4, 2, 6);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histo1.4.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;

title('alpha = 1.4');

subplot(4, 2, 7);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/laplass/histo1.5.txt");
values = data(:, 1);

histogram(values);
hold on;
grid on;

title('alpha = 1.5');

%% GAMMA

figure(10);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/normal0.1.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/normal0.6.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/normal1.5.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/normal6.0.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
xlim([0 255]);
hold on;
grid on;

title("Default gamma");
legend('0.1', '0.6', '1.5', '6');

figure(11);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/dark0.1.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/dark0.6.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/dark1.5.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/dark6.0.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;
xlim([0 255]);

title("Dark gamma");
legend('0.1', '0.6', '1.5', '6');

figure(12);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/light0.1.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/light0.6.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/light1.5.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;
xlim([0 255]);

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/light6.0.txt");
x = data(:, 1);
y = data(:, 2);

plot(x, y);
hold on;
grid on;

title("Light gamma");
legend('0.1', '0.6', '1.5', '6');


figure(13);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_light0.1.txt");
x = data(:, 1);

subplot(2,2,1);
histogram(x);
hold on;
grid on;
title('Light Gamma = 0.1');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_light0.6.txt");
x = data(:, 1);

subplot(2,2,2);
histogram(x);
hold on;
grid on;
title('Light Gamma = 0.6');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_light1.5.txt");
x = data(:, 1);

subplot(2,2,3);
histogram(x);
hold on;
grid on;
title('Light Gamma = 1.5');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_light6.0.txt");
x = data(:, 1);

subplot(2,2,4);
histogram(x);
hold on;
grid on;
title('Light Gamma = 6');


figure(14);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_dark0.1.txt");
x = data(:, 1);

subplot(2,2,1);
histogram(x);
hold on;
grid on;
title('Dark Gamma = 0.1');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_dark0.6.txt");
x = data(:, 1);

subplot(2,2,2);
histogram(x);
hold on;
grid on;
title('Dark Gamma = 0.6');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_dark1.5.txt");
x = data(:, 1);

subplot(2,2,3);
histogram(x);
hold on;
grid on;
title('Dark Gamma = 1.5');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_dark6.0.txt");
x = data(:, 1);

subplot(2,2,4);
histogram(x);
hold on;
grid on;
title('Dark Gamma = 6');


figure(15);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_normal0.1.txt");
x = data(:, 1);

subplot(2,2,1);
histogram(x);
hold on;
grid on;
title('Normal Gamma = 0.1');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_normal0.6.txt");
x = data(:, 1);

subplot(2,2,2);
histogram(x);
hold on;
grid on;
title('Normal Gamma = 0.6');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_normal1.5.txt");
x = data(:, 1);

subplot(2,2,3);
histogram(x);
hold on;
grid on;
title('Normal Gamma = 1.5');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/gammas/gist_normal6.0.txt");
x = data(:, 1);

subplot(2,2,4);
histogram(x);
hold on;
grid on;
title('Normal Gamma = 6');

figure(16);
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/align/dark.txt");
x = data(:, 1);

subplot(3,2,2)
histogram(x);
hold on;
grid on;
title('Align Dark');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/align/light.txt");
x = data(:, 1);

subplot(3,2,4)
histogram(x);
hold on;
grid on;
title('Align Light');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/align/normal.txt");
x = data(:, 1);

subplot(3,2,6)
histogram(x);
hold on;
grid on;
title('Align Normal');

subplot(3,2,1)
data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/align/origDark.txt");
x = data(:, 1);

histogram(x);
hold on;
grid on;
title('Dark');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/align/origLight.txt");
x = data(:, 1);

subplot(3,2,3)
histogram(x);
hold on;
grid on;
title('Light');

data = load("/Users/ivanrud/Desktop/suai/DIP/dip2/res/align/origNormal.txt");
x = data(:, 1);

subplot(3,2,5)
histogram(x);
hold on;
grid on;
title('Normal');

